/*
 * Name:       Ralph Williams
 * Course:     CSCI0013
 * Date:       4/16/2018
 * Filename:   Wxhw.java
 * Purpose:    File for creating a long string to extract JSON weather data from wunderground. 
               and then convert that data into Strings and print it out to the user.  
 */

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;

import com.google.gson.*;

// accessKey = ea202df8d65c8755;
// http://api.wunderground.com/api/ea202df8d65c8755/features/settings/q/query.format

public class Wxhw
{
	public String getWx(String zipCode)
	{
		JsonElement jse = null;
        String wxReport = null;
        String accessKey = "ea202df8d65c8755";
        String errorState;
                
		try
		{
			// Construct WxStation API URL
 			URL wxURL = new URL("http://api.wunderground.com/api/"
					+ accessKey + "/"
                    + "conditions/geolookup/q/"
					+ zipCode + ".json");        
                    
         // System.out.println(wxURL.toString());                                

			// Open the URL
			InputStream is = wxURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}


      // Find out if the data coming back is valid
      try {
      
         errorState = jse.getAsJsonObject().get("response")
                           .getAsJsonObject().get("error")
                           .getAsJsonObject().get("type").getAsString();
                           
      }
      catch (NullPointerException npe) {
         // The data coming threw a null pointer exception which means it does not have an error      
         errorState = "goodData";
      }

      if (errorState.equals("querynotfound"))  
         System.out.println("Error, invalid zip code entered, re-run the program with a valid zip code");
      
  
      // Before printing out the results of the weather report make sure that the JSON object is valid and the
      // data does not have an error in it.                           
      if ((jse != null) && (errorState == "goodData")) {
            
      // Build a weather report
      String cityState = jse.getAsJsonObject().get("current_observation")
                           .getAsJsonObject().get("observation_location")
                           .getAsJsonObject().get("full").getAsString();
                                       
      System.out.printf("%-20s%-40s%n", "Location: ", cityState);
      
      String observeTime = jse.getAsJsonObject().get("current_observation")
                              .getAsJsonObject().get("observation_time").getAsString();

      System.out.printf("%-20s%-40s%n", "Time: ", observeTime);
      
      String weatherReport = jse.getAsJsonObject().get("current_observation")
                                 .getAsJsonObject().get("weather").getAsString();

      System.out.printf("%-20s%-40s%n", "Weather: ", weatherReport);
      
      Float tempNumber = jse.getAsJsonObject().get("current_observation")
                              .getAsJsonObject().get("temp_f").getAsFloat();
                           
      System.out.printf("%-20s%-8.1f%n", "Temperature F: ", tempNumber);
                                                         
      String windReport = jse.getAsJsonObject().get("current_observation")
                              .getAsJsonObject().get("wind_string").getAsString();
                     
      System.out.printf("%-20s%-40s%n", "Wind: ", windReport);

      String pressureReport = jse.getAsJsonObject().get("current_observation")
                                 .getAsJsonObject().get("pressure_in").getAsString();

      System.out.printf("%-20s%-20s%n", "Pressure inHG: ", pressureReport);
      
      }
    return wxReport = "";
	}

	public static void main(String[] args)
	{
		Wxhw b = new Wxhw();
    if ( args.length == 0 )
      System.out.println("Please enter a Zip code as the first argument.");
    else
    {
		  String wx = b.getWx(args[0]);
      if ( wx != null )
		    System.out.println(wx);
    }
	}
}
